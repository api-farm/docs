# Выбор формата ответа

Большинство эндпойнтов веб-сервиса умеет возвращать ответы в двух форматах, XML и JSON. По умолчанию возвращается XML.

Выбрать формат JSON можно:

* указав параметр ```format=json``` в запросе или
* указав заголовок ```Accept: application/json```

Примеры (ссылки ведут на рабочий сервис):

* [GET /russian/declension?s=кошка&format=json](https://ws3.morpher.ru/russian/declension?s=кошка&format=json)
* [GET /russian/declension?s=кошка&format=xml](https://ws3.morpher.ru/russian/declension?s=кошка&format=xml)
