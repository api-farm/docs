# GET /qazaq/declension

Эндпойнт выполняет склонение по падежам, лицам и числам на казахском языке.
Для данного слова или фразы (параметр s) выдается полный набор падежно-числовых форм:

Ответ может возвращаться в XML или JSON. См. [выбор формата ответа](../../../response-format/index.md).

s – строка для склонения – должна быть в кодировке UTF-8 и перед вставкой в URL должна быть закодирована при помощи 
[URL Encoding](https://en.wikipedia.org/wiki/Percent-encoding).

Если слово уже во множественном, то тег <көпше> будет отсутствовать: қалалар.
 
```
GET /qazaq/declension?s=Нұрсултан Әбішұлы Назарбаев 
```

Варианты ответа

<table>
<thead>
<tr>
<th>xml</th><th>json</th>
</thead>
<tbody>
<tr>
<td>
<pre>
&lt;declension>
    &lt;A>Нұрсултан Әбішұлы Назарбаев&lt;/A>
    &lt;І>Нұрсултан Әбішұлы Назарбаевтың&lt;/І>
    &lt;Б>Нұрсултан Әбішұлы Назарбаевқа&lt;/Б>
    &lt;Т>Нұрсултан Әбішұлы Назарбаевты&lt;/Т>
    &lt;Ш>Нұрсултан Әбішұлы Назарбаевтан&lt;/Ш>
    &lt;Ж>Нұрсултан Әбішұлы Назарбаевта&lt;/Ж>
    &lt;К>Нұрсултан Әбішұлы Назарбаевпен&lt;/К>
    &lt;менің>
        &lt;A>Нұрсултан Әбішұлы Назарбаевым&lt;/A>
        &lt;І>Нұрсултан Әбішұлы Назарбаевымның&lt;/І>
        &lt;Б>Нұрсултан Әбішұлы Назарбаевыма&lt;/Б>
        &lt;Т>Нұрсултан Әбішұлы Назарбаевымды&lt;/Т>
        &lt;Ш>Нұрсултан Әбішұлы Назарбаевымнан&lt;/Ш>
        &lt;Ж>Нұрсултан Әбішұлы Назарбаевымда&lt;/Ж>
        &lt;К>Нұрсултан Әбішұлы Назарбаевыммен&lt;/К>
    &lt;/менің>
    &lt;сенің>
        &lt;A>Нұрсултан Әбішұлы Назарбаевың&lt;/A>
        &lt;І>Нұрсултан Әбішұлы Назарбаевыңның&lt;/І>
        &lt;Б>Нұрсултан Әбішұлы Назарбаевыңа&lt;/Б>
        &lt;Т>Нұрсултан Әбішұлы Назарбаевыңды&lt;/Т>
        &lt;Ш>Нұрсултан Әбішұлы Назарбаевыңнан&lt;/Ш>
        &lt;Ж>Нұрсултан Әбішұлы Назарбаевыңда&lt;/Ж>
        &lt;К>Нұрсултан Әбішұлы Назарбаевыңмен&lt;/К>
    &lt;/сенің>
    &lt;сіздің>
        &lt;A>Нұрсултан Әбішұлы Назарбаевыңыз&lt;/A>
        &lt;І>Нұрсултан Әбішұлы Назарбаевыңыздың&lt;/І>
        &lt;Б>Нұрсултан Әбішұлы Назарбаевыңызға&lt;/Б>
        &lt;Т>Нұрсултан Әбішұлы Назарбаевыңызды&lt;/Т>
        &lt;Ш>Нұрсултан Әбішұлы Назарбаевыңыздан&lt;/Ш>
        &lt;Ж>Нұрсултан Әбішұлы Назарбаевыңызда&lt;/Ж>
        &lt;К>Нұрсултан Әбішұлы Назарбаевыңызбен&lt;/К>
    &lt;/сіздің>
    &lt;оның>
        &lt;A>Нұрсултан Әбішұлы Назарбаевы&lt;/A>
        &lt;І>Нұрсултан Әбішұлы Назарбаевының&lt;/І>
        &lt;Б>Нұрсултан Әбішұлы Назарбаевына&lt;/Б>
        &lt;Т>Нұрсултан Әбішұлы Назарбаевын&lt;/Т>
        &lt;Ш>Нұрсултан Әбішұлы Назарбаевынан&lt;/Ш>
        &lt;Ж>Нұрсултан Әбішұлы Назарбаевында&lt;/Ж>
        &lt;К>Нұрсултан Әбішұлы Назарбаевымен&lt;/К>
    &lt;/оның>
    &lt;біздің>
        &lt;A>Нұрсултан Әбішұлы Назарбаевымыз&lt;/A>
        &lt;І>Нұрсултан Әбішұлы Назарбаевымыздың&lt;/І>
        &lt;Б>Нұрсултан Әбішұлы Назарбаевымызға&lt;/Б>
        &lt;Т>Нұрсултан Әбішұлы Назарбаевымызды&lt;/Т>
        &lt;Ш>Нұрсултан Әбішұлы Назарбаевымыздан&lt;/Ш>
        &lt;Ж>Нұрсултан Әбішұлы Назарбаевымызда&lt;/Ж>
        &lt;К>Нұрсултан Әбішұлы Назарбаевымызбен&lt;/К>
    &lt;/біздің>
    &lt;сендердің>
        &lt;A>Нұрсултан Әбішұлы Назарбаевтарың&lt;/A>
        &lt;І>Нұрсултан Әбішұлы Назарбаевтарыңның&lt;/І>
        &lt;Б>Нұрсултан Әбішұлы Назарбаевтарыңа&lt;/Б>
        &lt;Т>Нұрсултан Әбішұлы Назарбаевтарыңды&lt;/Т>
        &lt;Ш>Нұрсултан Әбішұлы Назарбаевтарыңнан&lt;/Ш>
        &lt;Ж>Нұрсултан Әбішұлы Назарбаевтарыңда&lt;/Ж>
        &lt;К>Нұрсултан Әбішұлы Назарбаевтарыңмен&lt;/К>
    &lt;/сендердің>
    &lt;сіздердің>
        &lt;A>Нұрсултан Әбішұлы Назарбаевтарыңыз&lt;/A>
        &lt;І>Нұрсултан Әбішұлы Назарбаевтарыңыздың&lt;/І>
        &lt;Б>Нұрсултан Әбішұлы Назарбаевтарыңызға&lt;/Б>
        &lt;Т>Нұрсултан Әбішұлы Назарбаевтарыңызды&lt;/Т>
        &lt;Ш>Нұрсултан Әбішұлы Назарбаевтарыңыздан&lt;/Ш>
        &lt;Ж>Нұрсултан Әбішұлы Назарбаевтарыңызда&lt;/Ж>
        &lt;К>Нұрсултан Әбішұлы Назарбаевтарыңызбен&lt;/К>
    &lt;/сіздердің>
    &lt;олардың>
        &lt;A>Нұрсултан Әбішұлы Назарбаевтары&lt;/A>
        &lt;І>Нұрсултан Әбішұлы Назарбаевтарының&lt;/І>
        &lt;Б>Нұрсултан Әбішұлы Назарбаевтарына&lt;/Б>
        &lt;Т>Нұрсултан Әбішұлы Назарбаевтарын&lt;/Т>
        &lt;Ш>Нұрсултан Әбішұлы Назарбаевтарынан&lt;/Ш>
        &lt;Ж>Нұрсултан Әбішұлы Назарбаевтарында&lt;/Ж>
        &lt;К>Нұрсултан Әбішұлы Назарбаевтарымен&lt;/К>
    &lt;/олардың>
    &lt;көпше>
        &lt;A>Нұрсултан Әбішұлы Назарбаевтар&lt;/A>
        &lt;І>Нұрсултан Әбішұлы Назарбаевтардың&lt;/І>
        &lt;Б>Нұрсултан Әбішұлы Назарбаевтарға&lt;/Б>
        &lt;Т>Нұрсултан Әбішұлы Назарбаевтарды&lt;/Т>
        &lt;Ш>Нұрсултан Әбішұлы Назарбаевтардан&lt;/Ш>
        &lt;Ж>Нұрсултан Әбішұлы Назарбаевтарда&lt;/Ж>
        &lt;К>Нұрсултан Әбішұлы Назарбаевтармен&lt;/К>
        &lt;менің>
            &lt;A>Нұрсултан Әбішұлы Назарбаевтарым&lt;/A>
            &lt;І>Нұрсултан Әбішұлы Назарбаевтарымның&lt;/І>
            &lt;Б>Нұрсултан Әбішұлы Назарбаевтарыма&lt;/Б>
            &lt;Т>Нұрсултан Әбішұлы Назарбаевтарымды&lt;/Т>
            &lt;Ш>Нұрсултан Әбішұлы Назарбаевтарымнан&lt;/Ш>
            &lt;Ж>Нұрсултан Әбішұлы Назарбаевтарымда&lt;/Ж>
            &lt;К>Нұрсултан Әбішұлы Назарбаевтарыммен&lt;/К>
        &lt;/менің>
        &lt;сенің>
            &lt;A>Нұрсултан Әбішұлы Назарбаевтарың&lt;/A>
            &lt;І>Нұрсултан Әбішұлы Назарбаевтарыңның&lt;/І>
            &lt;Б>Нұрсултан Әбішұлы Назарбаевтарыңа&lt;/Б>
            &lt;Т>Нұрсултан Әбішұлы Назарбаевтарыңды&lt;/Т>
            &lt;Ш>Нұрсултан Әбішұлы Назарбаевтарыңнан&lt;/Ш>
            &lt;Ж>Нұрсултан Әбішұлы Назарбаевтарыңда&lt;/Ж>
            &lt;К>Нұрсултан Әбішұлы Назарбаевтарыңмен&lt;/К>
        &lt;/сенің>
        &lt;сіздің>
            &lt;A>Нұрсултан Әбішұлы Назарбаевтарыңыз&lt;/A>
            &lt;І>Нұрсултан Әбішұлы Назарбаевтарыңыздың&lt;/І>
            &lt;Б>Нұрсултан Әбішұлы Назарбаевтарыңызға&lt;/Б>
            &lt;Т>Нұрсултан Әбішұлы Назарбаевтарыңызды&lt;/Т>
            &lt;Ш>Нұрсултан Әбішұлы Назарбаевтарыңыздан&lt;/Ш>
            &lt;Ж>Нұрсултан Әбішұлы Назарбаевтарыңызда&lt;/Ж>
            &lt;К>Нұрсултан Әбішұлы Назарбаевтарыңызбен&lt;/К>
        &lt;/сіздің>
        &lt;оның>
            &lt;A>Нұрсултан Әбішұлы Назарбаевтары&lt;/A>
            &lt;І>Нұрсултан Әбішұлы Назарбаевтарының&lt;/І>
            &lt;Б>Нұрсултан Әбішұлы Назарбаевтарына&lt;/Б>
            &lt;Т>Нұрсултан Әбішұлы Назарбаевтарын&lt;/Т>
            &lt;Ш>Нұрсултан Әбішұлы Назарбаевтарынан&lt;/Ш>
            &lt;Ж>Нұрсултан Әбішұлы Назарбаевтарында&lt;/Ж>
            &lt;К>Нұрсултан Әбішұлы Назарбаевтарымен&lt;/К>
        &lt;/оның>
        &lt;біздің>
            &lt;A>Нұрсултан Әбішұлы Назарбаевтарымыз&lt;/A>
            &lt;І>Нұрсултан Әбішұлы Назарбаевтарымыздың&lt;/І>
            &lt;Б>Нұрсултан Әбішұлы Назарбаевтарымызға&lt;/Б>
            &lt;Т>Нұрсултан Әбішұлы Назарбаевтарымызды&lt;/Т>
            &lt;Ш>Нұрсултан Әбішұлы Назарбаевтарымыздан&lt;/Ш>
            &lt;Ж>Нұрсултан Әбішұлы Назарбаевтарымызда&lt;/Ж>
            &lt;К>Нұрсултан Әбішұлы Назарбаевтарымызбен&lt;/К>
        &lt;/біздің>
        &lt;сендердің>
            &lt;A>Нұрсултан Әбішұлы Назарбаевтарың&lt;/A>
            &lt;І>Нұрсултан Әбішұлы Назарбаевтарыңның&lt;/І>
            &lt;Б>Нұрсултан Әбішұлы Назарбаевтарыңа&lt;/Б>
            &lt;Т>Нұрсултан Әбішұлы Назарбаевтарыңды&lt;/Т>
            &lt;Ш>Нұрсултан Әбішұлы Назарбаевтарыңнан&lt;/Ш>
            &lt;Ж>Нұрсултан Әбішұлы Назарбаевтарыңда&lt;/Ж>
            &lt;К>Нұрсултан Әбішұлы Назарбаевтарыңмен&lt;/К>
        &lt;/сендердің>
        &lt;сіздердің>
            &lt;A>Нұрсултан Әбішұлы Назарбаевтарыңыз&lt;/A>
            &lt;І>Нұрсултан Әбішұлы Назарбаевтарыңыздың&lt;/І>
            &lt;Б>Нұрсултан Әбішұлы Назарбаевтарыңызға&lt;/Б>
            &lt;Т>Нұрсултан Әбішұлы Назарбаевтарыңызды&lt;/Т>
            &lt;Ш>Нұрсултан Әбішұлы Назарбаевтарыңыздан&lt;/Ш>
            &lt;Ж>Нұрсултан Әбішұлы Назарбаевтарыңызда&lt;/Ж>
            &lt;К>Нұрсултан Әбішұлы Назарбаевтарыңызбен&lt;/К>
        &lt;/сіздердің>
        &lt;олардың>
            &lt;A>Нұрсултан Әбішұлы Назарбаевтары&lt;/A>
            &lt;І>Нұрсултан Әбішұлы Назарбаевтарының&lt;/І>
            &lt;Б>Нұрсултан Әбішұлы Назарбаевтарына&lt;/Б>
            &lt;Т>Нұрсултан Әбішұлы Назарбаевтарын&lt;/Т>
            &lt;Ш>Нұрсултан Әбішұлы Назарбаевтарынан&lt;/Ш>
            &lt;Ж>Нұрсултан Әбішұлы Назарбаевтарында&lt;/Ж>
            &lt;К>Нұрсултан Әбішұлы Назарбаевтарымен&lt;/К>
        &lt;/олардың>
    &lt;/көпше>
&lt;/declension>
</pre> 
</td>
<td>
<pre>
{
  "A": "Нұрсултан Әбішұлы Назарбаев",
  "І": "Нұрсултан Әбішұлы Назарбаевтың",
  "Б": "Нұрсултан Әбішұлы Назарбаевқа",
  "Т": "Нұрсултан Әбішұлы Назарбаевты",
  "Ш": "Нұрсултан Әбішұлы Назарбаевтан",
  "Ж": "Нұрсултан Әбішұлы Назарбаевта",
  "К": "Нұрсултан Әбішұлы Назарбаевпен",
  "менің": {
    "A": "Нұрсултан Әбішұлы Назарбаевым",
    "І": "Нұрсултан Әбішұлы Назарбаевымның",
    "Б": "Нұрсултан Әбішұлы Назарбаевыма",
    "Т": "Нұрсултан Әбішұлы Назарбаевымды",
    "Ш": "Нұрсултан Әбішұлы Назарбаевымнан",
    "Ж": "Нұрсултан Әбішұлы Назарбаевымда",
    "К": "Нұрсултан Әбішұлы Назарбаевыммен"
  },
  "сенің": {
    "A": "Нұрсултан Әбішұлы Назарбаевың",
    "І": "Нұрсултан Әбішұлы Назарбаевыңның",
    "Б": "Нұрсултан Әбішұлы Назарбаевыңа",
    "Т": "Нұрсултан Әбішұлы Назарбаевыңды",
    "Ш": "Нұрсултан Әбішұлы Назарбаевыңнан",
    "Ж": "Нұрсултан Әбішұлы Назарбаевыңда",
    "К": "Нұрсултан Әбішұлы Назарбаевыңмен"
  },
  "сіздің": {
    "A": "Нұрсултан Әбішұлы Назарбаевыңыз",
    "І": "Нұрсултан Әбішұлы Назарбаевыңыздың",
    "Б": "Нұрсултан Әбішұлы Назарбаевыңызға",
    "Т": "Нұрсултан Әбішұлы Назарбаевыңызды",
    "Ш": "Нұрсултан Әбішұлы Назарбаевыңыздан",
    "Ж": "Нұрсултан Әбішұлы Назарбаевыңызда",
    "К": "Нұрсултан Әбішұлы Назарбаевыңызбен"
  },
  "оның": {
    "A": "Нұрсултан Әбішұлы Назарбаевы",
    "І": "Нұрсултан Әбішұлы Назарбаевының",
    "Б": "Нұрсултан Әбішұлы Назарбаевына",
    "Т": "Нұрсултан Әбішұлы Назарбаевын",
    "Ш": "Нұрсултан Әбішұлы Назарбаевынан",
    "Ж": "Нұрсултан Әбішұлы Назарбаевында",
    "К": "Нұрсултан Әбішұлы Назарбаевымен"
  },
  "біздің": {
    "A": "Нұрсултан Әбішұлы Назарбаевымыз",
    "І": "Нұрсултан Әбішұлы Назарбаевымыздың",
    "Б": "Нұрсултан Әбішұлы Назарбаевымызға",
    "Т": "Нұрсултан Әбішұлы Назарбаевымызды",
    "Ш": "Нұрсултан Әбішұлы Назарбаевымыздан",
    "Ж": "Нұрсултан Әбішұлы Назарбаевымызда",
    "К": "Нұрсултан Әбішұлы Назарбаевымызбен"
  },
  "сендердің": {
    "A": "Нұрсултан Әбішұлы Назарбаевтарың",
    "І": "Нұрсултан Әбішұлы Назарбаевтарыңның",
    "Б": "Нұрсултан Әбішұлы Назарбаевтарыңа",
    "Т": "Нұрсултан Әбішұлы Назарбаевтарыңды",
    "Ш": "Нұрсултан Әбішұлы Назарбаевтарыңнан",
    "Ж": "Нұрсултан Әбішұлы Назарбаевтарыңда",
    "К": "Нұрсултан Әбішұлы Назарбаевтарыңмен"
  },
  "сіздердің": {
    "A": "Нұрсултан Әбішұлы Назарбаевтарыңыз",
    "І": "Нұрсултан Әбішұлы Назарбаевтарыңыздың",
    "Б": "Нұрсултан Әбішұлы Назарбаевтарыңызға",
    "Т": "Нұрсултан Әбішұлы Назарбаевтарыңызды",
    "Ш": "Нұрсултан Әбішұлы Назарбаевтарыңыздан",
    "Ж": "Нұрсултан Әбішұлы Назарбаевтарыңызда",
    "К": "Нұрсултан Әбішұлы Назарбаевтарыңызбен"
  },
  "олардың": {
    "A": "Нұрсултан Әбішұлы Назарбаевтары",
    "І": "Нұрсултан Әбішұлы Назарбаевтарының",
    "Б": "Нұрсултан Әбішұлы Назарбаевтарына",
    "Т": "Нұрсултан Әбішұлы Назарбаевтарын",
    "Ш": "Нұрсултан Әбішұлы Назарбаевтарынан",
    "Ж": "Нұрсултан Әбішұлы Назарбаевтарында",
    "К": "Нұрсултан Әбішұлы Назарбаевтарымен"
  },
  "көпше": {
    "A": "Нұрсултан Әбішұлы Назарбаевтар",
    "І": "Нұрсултан Әбішұлы Назарбаевтардың",
    "Б": "Нұрсултан Әбішұлы Назарбаевтарға",
    "Т": "Нұрсултан Әбішұлы Назарбаевтарды",
    "Ш": "Нұрсултан Әбішұлы Назарбаевтардан",
    "Ж": "Нұрсултан Әбішұлы Назарбаевтарда",
    "К": "Нұрсултан Әбішұлы Назарбаевтармен",
    "менің": {
      "A": "Нұрсултан Әбішұлы Назарбаевтарым",
      "І": "Нұрсултан Әбішұлы Назарбаевтарымның",
      "Б": "Нұрсултан Әбішұлы Назарбаевтарыма",
      "Т": "Нұрсултан Әбішұлы Назарбаевтарымды",
      "Ш": "Нұрсултан Әбішұлы Назарбаевтарымнан",
      "Ж": "Нұрсултан Әбішұлы Назарбаевтарымда",
      "К": "Нұрсултан Әбішұлы Назарбаевтарыммен"
    },
    "сенің": {
      "A": "Нұрсултан Әбішұлы Назарбаевтарың",
      "І": "Нұрсултан Әбішұлы Назарбаевтарыңның",
      "Б": "Нұрсултан Әбішұлы Назарбаевтарыңа",
      "Т": "Нұрсултан Әбішұлы Назарбаевтарыңды",
      "Ш": "Нұрсултан Әбішұлы Назарбаевтарыңнан",
      "Ж": "Нұрсултан Әбішұлы Назарбаевтарыңда",
      "К": "Нұрсултан Әбішұлы Назарбаевтарыңмен"
    },
    "сіздің": {
      "A": "Нұрсултан Әбішұлы Назарбаевтарыңыз",
      "І": "Нұрсултан Әбішұлы Назарбаевтарыңыздың",
      "Б": "Нұрсултан Әбішұлы Назарбаевтарыңызға",
      "Т": "Нұрсултан Әбішұлы Назарбаевтарыңызды",
      "Ш": "Нұрсултан Әбішұлы Назарбаевтарыңыздан",
      "Ж": "Нұрсултан Әбішұлы Назарбаевтарыңызда",
      "К": "Нұрсултан Әбішұлы Назарбаевтарыңызбен"
    },
    "оның": {
      "A": "Нұрсултан Әбішұлы Назарбаевтары",
      "І": "Нұрсултан Әбішұлы Назарбаевтарының",
      "Б": "Нұрсултан Әбішұлы Назарбаевтарына",
      "Т": "Нұрсултан Әбішұлы Назарбаевтарын",
      "Ш": "Нұрсултан Әбішұлы Назарбаевтарынан",
      "Ж": "Нұрсултан Әбішұлы Назарбаевтарында",
      "К": "Нұрсултан Әбішұлы Назарбаевтарымен"
    },
    "біздің": {
      "A": "Нұрсултан Әбішұлы Назарбаевтарымыз",
      "І": "Нұрсултан Әбішұлы Назарбаевтарымыздың",
      "Б": "Нұрсултан Әбішұлы Назарбаевтарымызға",
      "Т": "Нұрсултан Әбішұлы Назарбаевтарымызды",
      "Ш": "Нұрсултан Әбішұлы Назарбаевтарымыздан",
      "Ж": "Нұрсултан Әбішұлы Назарбаевтарымызда",
      "К": "Нұрсултан Әбішұлы Назарбаевтарымызбен"
    },
    "сендердің": {
      "A": "Нұрсултан Әбішұлы Назарбаевтарың",
      "І": "Нұрсултан Әбішұлы Назарбаевтарыңның",
      "Б": "Нұрсултан Әбішұлы Назарбаевтарыңа",
      "Т": "Нұрсултан Әбішұлы Назарбаевтарыңды",
      "Ш": "Нұрсултан Әбішұлы Назарбаевтарыңнан",
      "Ж": "Нұрсултан Әбішұлы Назарбаевтарыңда",
      "К": "Нұрсултан Әбішұлы Назарбаевтарыңмен"
    },
    "сіздердің": {
      "A": "Нұрсултан Әбішұлы Назарбаевтарыңыз",
      "І": "Нұрсултан Әбішұлы Назарбаевтарыңыздың",
      "Б": "Нұрсултан Әбішұлы Назарбаевтарыңызға",
      "Т": "Нұрсултан Әбішұлы Назарбаевтарыңызды",
      "Ш": "Нұрсултан Әбішұлы Назарбаевтарыңыздан",
      "Ж": "Нұрсултан Әбішұлы Назарбаевтарыңызда",
      "К": "Нұрсултан Әбішұлы Назарбаевтарыңызбен"
    },
    "олардың": {
      "A": "Нұрсултан Әбішұлы Назарбаевтары",
      "І": "Нұрсултан Әбішұлы Назарбаевтарының",
      "Б": "Нұрсултан Әбішұлы Назарбаевтарына",
      "Т": "Нұрсултан Әбішұлы Назарбаевтарын",
      "Ш": "Нұрсултан Әбішұлы Назарбаевтарынан",
      "Ж": "Нұрсултан Әбішұлы Назарбаевтарында",
      "К": "Нұрсултан Әбішұлы Назарбаевтарымен"
    }
  }
}
</pre>                                     
</td>
</tr>
</tbody>
</table>


 
