# DELETE /ukrainian/userdict

Эндпойнт удалить исправления.

 Для того чтобы удалить исправление, достаточно передать DELETE запрос вида:

```http request
DELETE /ukrainian/userdict?s="Строка в именительном падеже"
```

Например, чтобы удалить все исправления для слова "Кошка", нужно передать:

```http request
DELETE /ukrainian/userdict?s=Кошка
```