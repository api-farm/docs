# GET /ukrainian/spell

Эндпойнт  решает две родственные задачи: получение прописи числа (тысяча сто двадцать пять) и согласование единицы измерения с предшествующем числом (1 попугай, 2 попугая, 5 попугаев): 

Ответ может возвращаться в XML или JSON. См. [выбор формата ответа](../../../response-format/index.md).

```
https://ws3.morpher.ru/ukrainian/spell?n=235&unit=рубль
```

Ответ

<table>
<thead>
<tr>
<th>xml</th><th>json</th>
</thead>
<tbody>
<tr>
<td>
<pre>
&lt;PropisUkrResult>
    &lt;n>
        &lt;Н>двісті тридцять п'ять&lt;/Н>
        &lt;Р>двохсот тридцяти п'яти&lt;/Р>
        &lt;Д>двомстам тридцяти п'яти&lt;/Д>
        &lt;З>двісті тридцять п'ять&lt;/З>
        &lt;О>двомастами тридцятьма п'ятьма&lt;/О>
        &lt;М>двохстах тридцяти п'яти&lt;/М>
        &lt;К>двісті тридцять п'ять&lt;/К>
    &lt;/n>
    &lt;unit>
        &lt;Н>рублів&lt;/Н>
        &lt;Р>рублів&lt;/Р>
        &lt;Д>рублям&lt;/Д>
        &lt;З>рублів&lt;/З>
        &lt;О>рублями&lt;/О>
        &lt;М>рублях&lt;/М>
        &lt;К>рублів&lt;/К>
    &lt;/unit>
&lt;/PropisUkrResult>
</pre> 
</td>
<td>
<pre>
{
  "n": {
    "Н": "двісті тридцять п'ять",
    "Р": "двохсот тридцяти п'яти",
    "Д": "двомстам тридцяти п'яти",
    "З": "двісті тридцять п'ять",
    "О": "двомастами тридцятьма п'ятьма",
    "М": "двохстах тридцяти п'яти",
    "К": "двісті тридцять п'ять"
  },
  "unit": {
    "Н": "рублів",
    "Р": "рублів",
    "Д": "рублям",
    "З": "рублів",
    "О": "рублями",
    "М": "рублях",
    "К": "рублів"
  }
}
</pre>                                     
</td>
</tr>
</tbody>
</table>

При ответе  в формате xml ответ соответствует следующей xsd схеме

```
<xs:schema elementFormDefault="qualified">
    <xs:element name="PropisResult" nillable="true" type="NumberSpelling"/>
    <xs:complexType name="NumberSpelling">
        <xs:sequence>
            <xs:element minOccurs="1" maxOccurs="1" name="n" type="DeclensionForms"/>
            <xs:element minOccurs="1" maxOccurs="1" name="unit" type="DeclensionForms"/>
        </xs:sequence>
    </xs:complexType>
    <xs:complexType name="DeclensionForms">
        <xs:sequence>
            <xs:element minOccurs="0" maxOccurs="1" name="И" type="xs:string"/>
            <xs:element minOccurs="0" maxOccurs="1" name="Р" type="xs:string"/>
            <xs:element minOccurs="0" maxOccurs="1" name="Д" type="xs:string"/>
            <xs:element minOccurs="0" maxOccurs="1" name="В" type="xs:string"/>
            <xs:element minOccurs="0" maxOccurs="1" name="Т" type="xs:string"/>
            <xs:element minOccurs="0" maxOccurs="1" name="П" type="xs:string"/>
        </xs:sequence>
    </xs:complexType>
</xs:schema>
```

Комбинируя соответствующие падежные формы n и unit, можно получить вывод «суммы прописью» на любой вкус:

 + 235 рублів
 + двісті тридцять п'ять рублів
 + 235 (двісті тридцять п'ять) рублів и т.п.

Комбинировать имеет смысл только соответствующие падежные формы n и unit.

См. также примеры в описании функции [Пропись из Morpher.dll для .NET](http://morpher.ru/dotnet/#propis), которая лежит в основе этого веб-сервиса. 