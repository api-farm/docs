# GET /russian/spell-ordinal

Эндпойнт похож на функцию /spell, но возвращает пропись числа в форме порядкового числительного.
Ответ может возвращаться в XML или JSON. См. [выбор формата ответа](../../../response-format/index.md).

```
GET /russian/spell-ordinal?n=5&unit=колесо
```

Ответ

<table>
<thead>
<tr>
<th>xml</th><th>json</th>
</thead>
<tbody>
<tr>
<td>
<pre>
&lt;PropisResult>       
   &lt;n>               
       &lt;И>пятое&lt;/И>  
       &lt;Р>пятого&lt;/Р> 
       &lt;Д>пятому&lt;/Д> 
       &lt;В>пятое&lt;/В>  
       &lt;Т>пятым&lt;/Т>  
       &lt;П>пятом&lt;/П>  
   &lt;/n>              
   &lt;unit>            
       &lt;И>колесо&lt;/И> 
       &lt;Р>колеса&lt;/Р> 
       &lt;Д>колесу&lt;/Д> 
       &lt;В>колесо&lt;/В> 
       &lt;Т>колесом&lt;/Т>
       &lt;П>колесе&lt;/П> 
   &lt;/unit>           
 &lt;/PropisResult>
</pre> 
</td>
<td>
<pre>
{                    
  "n": {             
      "И": "пятое",  
      "Р": "пятого", 
      "Д": "пятому", 
      "В": "пятое",  
      "Т": "пятым",  
      "П": "пятом"   
  },                 
  "unit": {          
      "И": "колесо", 
      "Р": "колеса", 
      "Д": "колесу", 
      "В": "колесо", 
      "Т": "колесом",
      "П": "колесе"  
  }                  
}  
</pre>                                     
</td>
</tr>
</tbody>
</table>

При ответе в формате xml ответ соответствует следующей xsd схеме:

```xml
<xs:schema elementFormDefault="qualified">
    <xs:element name="PropisResult" nillable="true" type="NumberSpelling"/>
    <xs:complexType name="NumberSpelling">
        <xs:sequence>
            <xs:element minOccurs="1" maxOccurs="1" name="n" type="DeclensionForms"/>
            <xs:element minOccurs="1" maxOccurs="1" name="unit" type="DeclensionForms"/>
        </xs:sequence>
    </xs:complexType>
    <xs:complexType name="DeclensionForms">
        <xs:sequence>
            <xs:element minOccurs="0" maxOccurs="1" name="И" type="xs:string"/>
            <xs:element minOccurs="0" maxOccurs="1" name="Р" type="xs:string"/>
            <xs:element minOccurs="0" maxOccurs="1" name="Д" type="xs:string"/>
            <xs:element minOccurs="0" maxOccurs="1" name="В" type="xs:string"/>
            <xs:element minOccurs="0" maxOccurs="1" name="Т" type="xs:string"/>
            <xs:element minOccurs="0" maxOccurs="1" name="П" type="xs:string"/>
        </xs:sequence>
    </xs:complexType>
</xs:schema>
```
 