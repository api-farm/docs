# GET /russian/spell

Эндпойнт  решает две родственные задачи: получение прописи числа (тысяча сто двадцать пять) и согласование единицы измерения с предшествующем числом (1 попугай, 2 попугая, 5 попугаев): 

Ответ может возвращаться в XML или JSON. См. [выбор формата ответа](../../../response-format/index.md).

```
https://ws3.morpher.ru/russian/spell?n=235&unit=рубль
```

Ответ

<table>
<thead>
<tr>
<th>xml</th><th>json</th>
</thead>
<tbody>
<tr>
<td>
<pre>
&lt;PropisResult>                          
   &lt;n>         
       <И>двести тридцать пять&lt;/И>
       <Р>двухсот тридцати пяти&lt;/Р>
       <Д>двумстам тридцати пяти&lt;/Д>
       <В>двести тридцать пять&lt;/В>
       <Т>двумястами тридцатью пятью&lt;/Т>
       <П>двухстах тридцати пяти&lt;/П>
   &lt;/n>
   &lt;unit>
       <И>рублей&lt;/И>
       <Р>рублей&lt;/Р>
       <Д>рублям&lt;/Д>
       <В>рублей&lt;/В>
       <Т>рублями&lt;/Т>             
       <П>рублях&lt;/П>                    
   &lt;/unit>
 &lt;/PropisResult>                         
</pre> 
</td>
<td>
<pre>
{
     "n": {  
         "И": "двести тридцать пять",
         "Р": "двухсот тридцати пяти",
         "Д": "двумстам тридцати пяти",
         "В": "двести тридцать пять",      
         "Т": "двумястами тридцатью пятью",
         "П": "двухстах тридцати пяти"
     },     
     "unit": {    
         "И": "рублей", 
         "Р": "рублей", 
         "Д": "рублям",
         "В": "рублей",
         "Т": "рублями",
         "П": "рублях"
     }                
}         
</pre>                                     
</td>
</tr>
</tbody>
</table>

При ответе  в формате xml ответ соответствует следующей xsd схеме

```
<xs:schema elementFormDefault="qualified">
    <xs:element name="PropisResult" nillable="true" type="NumberSpelling"/>
    <xs:complexType name="NumberSpelling">
        <xs:sequence>
            <xs:element minOccurs="1" maxOccurs="1" name="n" type="DeclensionForms"/>
            <xs:element minOccurs="1" maxOccurs="1" name="unit" type="DeclensionForms"/>
        </xs:sequence>
    </xs:complexType>
    <xs:complexType name="DeclensionForms">
        <xs:sequence>
            <xs:element minOccurs="0" maxOccurs="1" name="И" type="xs:string"/>
            <xs:element minOccurs="0" maxOccurs="1" name="Р" type="xs:string"/>
            <xs:element minOccurs="0" maxOccurs="1" name="Д" type="xs:string"/>
            <xs:element minOccurs="0" maxOccurs="1" name="В" type="xs:string"/>
            <xs:element minOccurs="0" maxOccurs="1" name="Т" type="xs:string"/>
            <xs:element minOccurs="0" maxOccurs="1" name="П" type="xs:string"/>
        </xs:sequence>
    </xs:complexType>
</xs:schema>
```

Комбинируя соответствующие падежные формы n и unit, можно получить вывод «суммы прописью» на любой вкус:

 + 235 рублей
 + Двести тридцать пять рублей
 + 235 (двести тридцать пять) рублей и т.п.

Комбинировать имеет смысл только соответствующие падежные формы n и unit – именительный с именительным, родительный с родительным и т.д. 
Например, для получения фразы "в размере N рублей (долларов, евро...)", берем родительный падеж (<Р>): в размере + двухсот тридцати пяти + рублей.

См. также примеры в описании функции [Пропись из Morpher.dll для .NET](http://morpher.ru/dotnet/#propis), которая лежит в основе этого веб-сервиса. 