# Склонение по падежам на русском языке

Чтобы просклонять слово по падежам, вызовите эндпойнт:

 * [GET /russian/declension](GET/)

## Пользовательский словарь

Для коррекции склонения отдельных фраз:

 * [GET /russian/userdict](../userdict/GET/) - Получить список всех добавленных исправлений
 * [POST /russian/userdict](../userdict/POST/) - Добавить или изменить исправление
 * [DELETE /russian/userdict](../userdict/DELETE/) - Удалить исправление

См. также функции склонения по падежам на 
[украинском](../../ukrainian/declension/index.md) и
[казахском](../../qazaq/declension/index.md) языках.