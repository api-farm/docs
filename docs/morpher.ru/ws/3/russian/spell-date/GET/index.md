# GET /russian/spell-date

Эндпойнт склоняет по падежам дату, заданную в формате ГГГГ-ММ-ДД.
Ответ может возвращаться в XML или JSON. См. [выбор формата ответа](../../../response-format/index.md).

```
https://ws3.morpher.ru/russian/spell-date?date=2019-06-29 
```

Ответ

<table>
<thead>
<tr>
<th>xml</th><th>json</th>
</thead>
<tbody>
<tr>
<td>
<pre>
&lt;PropisResult>  
    &lt;И>двадцать девятое июня две тысячи девятнадцатого года&lt;/И>                                           
    &lt;Р>двадцать девятого июня две тысячи девятнадцатого года&lt;/Р>                  
    &lt;Д>двадцать девятому июня две тысячи девятнадцатого года&lt;/Д>
    &lt;В>двадцать девятое июня две тысячи девятнадцатого года&lt;/В>
    &lt;Т>двадцать девятым июня две тысячи девятнадцатого года&lt;/Т>
    &lt;П>двадцать девятом июня две тысячи девятнадцатого года&lt;/П>
&lt;/PropisResult>
</pre> 
</td>
<td>
<pre>
{
    "И": "двадцать девятое июня две тысячи девятнадцатого года",
    "Р": "двадцать девятого июня две тысячи девятнадцатого года",
    "Д": "двадцать девятому июня две тысячи девятнадцатого года",
    "В": "двадцать девятое июня две тысячи девятнадцатого года",
    "Т": "двадцать девятым июня две тысячи девятнадцатого года",
    "П": "двадцать девятом июня две тысячи девятнадцатого года"
 }    
</pre>                                     
</td>
</tr>
</tbody>
</table>
