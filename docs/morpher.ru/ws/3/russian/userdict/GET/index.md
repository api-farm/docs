# GET /russian/userdict

Эндпойнт выдаёт список пользовательских исправлений.

Для того чтобы получить список всех исправлений, нужно послать GET запрос на /russian/userdict, указав токен.  

Ответ может возвращаться в XML или JSON. См. [выбор формата ответа](../../../response-format/index.md).

Пример ответа:

```xml
<?xml version="1.0" encoding="utf-8"?>
<dictionary>
    <Entry>
        <singular>
            <И>Кошка</И>
            <Р>Пантеры</Р>
            <Д>Пантере</Д>
        </singular>
    </Entry>
<dictionary>
```
XSD ответа:
```xml
<xs:schema>
	<xs:simpleType name="form">
		<xs:restriction base="xs:string"/>
	</xs:simpleType>
	<xs:simpleType name="gender">
		<xs:restriction base="xs:string">
			<xs:enumeration value="masculine">
				<xs:annotation>
					<xs:documentation xml:lang="ru">
						Мужской род (он мой): папа, сын, тесть.
					</xs:documentation>
				</xs:annotation>
			</xs:enumeration>
			<xs:enumeration value="feminine">
				<xs:annotation>
					<xs:documentation xml:lang="ru">
						Женский род (она моя): мама, дочь, кошка.
					</xs:documentation>
				</xs:annotation>
			</xs:enumeration>
			<xs:enumeration value="neuter">
				<xs:annotation>
					<xs:documentation xml:lang="ru">
						Средний род (оно моё): окно, море, копьё.
					</xs:documentation>
				</xs:annotation>
			</xs:enumeration>
			<xs:enumeration value="plural">
				<xs:annotation>
					<xs:documentation xml:lang="ru">
						Множественное число (они мои): очки, часы, Канары, Чебоксары.
					</xs:documentation>
				</xs:annotation>
			</xs:enumeration>
		</xs:restriction>
	</xs:simpleType>
	<xs:complexType name="single-number-forms">
		<xs:sequence>
			<xs:element name="И" minOccurs="0" maxOccurs="1" type="form"/>
			<xs:element name="Р" minOccurs="0" maxOccurs="1" type="form"/>
			<xs:element name="Д" minOccurs="0" maxOccurs="1" type="form"/>
			<xs:element name="В" minOccurs="0" maxOccurs="1" type="form"/>
			<xs:element name="Т" minOccurs="0" maxOccurs="1" type="form"/>
			<xs:element name="П" minOccurs="0" maxOccurs="1" type="form">
				<xs:annotation>
					<xs:documentation xml:lang="ru">
						Форма предложного падежа с предлогом О, ОБ или ОБО. Предлог может отсутствовать.
					</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="М" minOccurs="0" maxOccurs="1" type="form">
				<xs:annotation>
					<xs:documentation xml:lang="ru">
						Форма предложного падежа с предлогом В, ВО или НА, отвечает на вопрос где?: в городе, в лесу, на берегу, на площади, во Франции.
					</xs:documentation>
				</xs:annotation>
			</xs:element>
		</xs:sequence>
	</xs:complexType>
	<xs:element name="dictionary">
		<xs:complexType>
			<xs:choice minOccurs="0" maxOccurs="unbounded">
				<xs:element name="entry">
					<xs:complexType>
						<xs:sequence>
							<xs:element minOccurs="0" maxOccurs="1" type="single-number-forms" name="singular"/>
							<xs:element minOccurs="0" maxOccurs="1" type="single-number-forms" name="plural"/>
						</xs:sequence>
						<xs:attribute name="gender" type="gender"/>
					</xs:complexType>
				</xs:element>
			</xs:choice>
		</xs:complexType>
	</xs:element>
</xs:schema>
```